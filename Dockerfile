FROM quay.io/jupyter/datascience-notebook:hub-4.1.5

USER root

# Install ETHZ root ca certificates
ADD http://pkiaia.ethz.ch/aia/ETHZRootCA2020.pem /usr/local/share/ca-certificates/ETHZRootCA2020.crt
ADD http://pkiaia.ethz.ch/aia/ETHZIssuingCA2020.pem /usr/local/share/ca-certificates/ETHZIssuingCA2020.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootCA.pem /usr/local/share/ca-certificates/DigiCertGlobalRootCA.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertTLSRSASHA2562020CA1-1.pem /usr/local/share/ca-certificates/DigiCertTLSRSASHA2562020CA1-1.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootG2.pem /usr/local/share/ca-certificates/DigiCertGlobalRootG2.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalG2TLSRSASHA2562020CA1.pem /usr/local/share/ca-certificates/DigiCertGlobalG2TLSRSASHA2562020CA1.crt
RUN chmod 644 /usr/local/share/ca-certificates/* && update-ca-certificates

########################
# Basic OS related stuff
########################

# Fix timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >> /etc/timezone

# OS update, basic addon commands, and Owncloud client (polybox)
RUN apt-get update && apt-get upgrade -y && \
  apt-get install -y \
    acl \
    cmake \
    gpg \
    less \
    libarmadillo-dev \
    libcurl4-openssl-dev \
    libgdal-dev \
    libgeos-dev \
    libgl1-mesa-glx \
    libglpk-dev \
    libglu1-mesa \
    libgtk-3-0 \
    libmagick++-dev \
    libproj-dev \
    libssl-dev \
    libudunits2-dev \
    libxft2 \
    libxdamage-dev \
    lmodern \
    man \
    python3-pip \
    povray \
    texlive-xetex \
    texlive-lang-german \
    texlive-science \
    tesseract-ocr \
    tesseract-ocr-eng \
    tesseract-ocr-deu \
    vim \
    xvfb \
    zip \
    && \
  wget -nv https://download.owncloud.com/desktop/ownCloud/stable/latest/linux/Ubuntu_22.04/Release.key -O - | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/owncloud.gpg > /dev/null && \
  echo 'deb https://download.owncloud.com/desktop/ownCloud/stable/latest/linux/Ubuntu_22.04/ /' | sudo tee -a /etc/apt/sources.list.d/owncloud.list && \
  apt-get update && \
  apt-get install -y owncloud-client && \
  apt-get clean

#########################
# Jupyter base components
#########################
# Disable announcement popup
RUN jupyter labextension disable "@jupyterlab/apputils-extension:announcements"

# Globally required stuff, independent of programming language
 RUN mamba update --all \
  && \
  mamba install nbgitpuller jupyterlab-git jupyterlab-vpython jupyter-ai jupyterlab-deck jupyterlab-spreadsheet-editor \
  && \
  mamba clean --all

# Replace faulty compilers for R packages
RUN sed -i 's/x86_64-conda-linux-gnu-cc/gcc/g' /opt/conda/lib/R/etc/Makeconf && sed -i 's/x86_64-conda-linux-gnu-c++/c++/g' /opt/conda/lib/R/etc/Makeconf 

USER 1000
